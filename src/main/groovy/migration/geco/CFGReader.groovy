package migration.geco

import java.io.File;
import java.util.List;

class CFGReader{

	List readLine(String line){
		 def url=line.substring(0, line.indexOf("|"));
		 def revision=line.substring(line.indexOf("|") + 1, line.length() - 3);
		 return [url,revision.toLong()]
	}
	
	List readFile(File cfgFile){
		def cfg=[]
		def lines = cfgFile.readLines()
		for (line in lines) {
			def urlAndRevision=readLine(line)
			cfg<<urlAndRevision
		}
		return cfg
	}
}