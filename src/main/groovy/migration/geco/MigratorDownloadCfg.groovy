package migration.geco

import java.io.File;
import groovy.io.FileType
import org.apache.commons.io.FilenameUtils

class MigratorDownloadCfg {

	def workingDir
	def cfgDownloader

	MigratorDownloadCfg(workingDir,cfgDownloader){
		this.workingDir=workingDir
		this.cfgDownloader=cfgDownloader
	}
	
	void downloadAllProyects(File cfgsDir){
		cfgsDir.eachFileRecurse(FileType.FILES) { file ->
			if (isGecoFile(file)){
				def baseName=FilenameUtils.getBaseName(file.name)
				println "Iniciando descarga $baseName"
				try{
					def destDir=new File(workingDir,baseName)
					if(!destDir.exists()){
						destDir.mkdir()
						cfgDownloader.downloadProyect(destDir, file)
						println "Descarga terminada"
					}
					else{
						println "Proyecto no descargado, ya existe el directorio"
					}
				}
				catch(e){
					println "Error descargando el proyecto $baseName"
					e.printStackTrace()
				}
			}
		}
	}
	
	def isGecoFile(file){
		return FilenameUtils.getExtension(file.name).equals("cfg") ||
		FilenameUtils.getExtension(file.name).equals("lb")
	}
}
