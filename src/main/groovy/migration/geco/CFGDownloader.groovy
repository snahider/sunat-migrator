package migration.geco

import java.io.File;
import migration.repository.SvnRepository
import org.tmatesoft.svn.core.SVNException

class CFGDownloader{
	
	private CFGReader cfgreader;
	private SvnRepository svnRepository;
	private DirectoryTree directoryTree

	def CFGDownloader(svnRepository, directoryTree){
		this.svnRepository = svnRepository;
		this.directoryTree=directoryTree
		this.cfgreader = new CFGReader();
	}
	
	def downloadProyect(File destDir,File cfgFile){
		def cfg=cfgreader.readFile(cfgFile);
		for (line in cfg) {
			println line
			def url=line[0]
			def revision=line[1]
			def parentDir=directoryTree.create(destDir,url)
			if(!excludedDirectories(url)){
				try{
					svnRepository.export(url,revision,parentDir)
				}
				catch(SVNException e){
					println "Error URL $url doesnt exist"
					e.printStackTrace()
				}
			}
		}
	}
	
	def excludedDirectories(String url){
		return url.endsWith("build/classes") || 
			   url.endsWith("build/lib") ||
			   url.endsWith("build/merge")
	}
	
}