package migration.geco

import java.io.File;

class DirectoryTree{
	String repositoryPath
	DirectoryTree(repositoryPath){
		this.repositoryPath=addEndingSlash(repositoryPath)
	}
	
	def create(destDir,fullUrlPath){
		def lastDirectoryTreePath=lastDirectoryTreePath(fullUrlPath)
		def directoryTree=new File(destDir,lastDirectoryTreePath)
		directoryTree.mkdirs()
		return directoryTree
	}
	
	def lastDirectoryTreePath(String fullUrlPath){
		String fullRepositoryPath=repositoryPath;
		if(hasPort(fullUrlPath) && !hasPort(repositoryPath))
			fullRepositoryPath=addPort(repositoryPath)
		return fullUrlPath.substring(fullRepositoryPath.length(), fullUrlPath.lastIndexOf("/"))
	}
	
	def addPort(String url){
		URL uri = new URL(url);
		String protocol=uri.getProtocol();
		String host=uri.getHost();
		String path=uri.getPath();
		return "$protocol://$host:80$path"
	}
	
	def hasPort(String url){
		URL uri = new URL(url);
		int port=uri.getPort();
		return port!=-1;
	}
	
	def addEndingSlash(String path){
		if (!path.endsWith("/")) {
			return "$path/"
		}
		return path
	}
}
