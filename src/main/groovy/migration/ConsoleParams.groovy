package migration

import org.kohsuke.args4j.Argument
import org.kohsuke.args4j.Option

class ConsoleParams {
	
	@Argument
	List<String> arguments = new ArrayList<String>();
	
	@Option(name="-svnUser")
	String svnUser;

	@Option(name="-svnPass")
	String svnPassword;

	@Option(name="-gitUser")
	String gitUser;

	@Option(name="-gitPass")
	String gitPassword;
	
	@Option(name="-inputFile")
	String inputFile;

	boolean isCreateGitRepository(){
		return arguments[0]=="createRepository"
	}

	boolean isUpdateGitRepository(){
		return arguments[0]=="updateRepository"
	}
	
	@Option(name="-depIn")
	String dependencyInput;
	
	@Option(name="-depOut")
	String dependencyOutput;
	
	boolean isDependencies(){
		return arguments[0]=="dependencias"
	}
	
	boolean isVersiones(){
		return arguments[0]=="versiones"
	}
	
	boolean isScript(){
		return arguments[0]=="script"
	}

	@Option(name="-svnUrl")
	String svnUrl;
		
	@Option(name="-cfgsDir")
	String cfgsDir;
	
	boolean isDownloadCfg(){
		return arguments[0]=="downloadCfg"
	}
}
