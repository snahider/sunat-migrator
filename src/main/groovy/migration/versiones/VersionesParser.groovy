package migration.versiones

import java.io.File;

import com.google.common.base.CaseFormat

class VersionesParser{
	void parseFile(File inputFile,File outputFile){
		def project = new XmlParser().parse(inputFile)
		def text=parse(project);
		outputFile << text
	}

	String parseText(String propertiesMaven){
		def project = new XmlParser().parseText(propertiesMaven)
		return parse(project);
	}
	
	String parse(project){
		def properties=""
		if(project.properties.size()>0){
			if(project.properties[0].children().size()>0){
				properties=parseAllProperties(project.properties[0].children())
			}
		}
		return properties
	}
	
	def parseAllProperties(mvnProperties){
		def properties=""
		mvnProperties.each{ child ->
			def property = parseSingleProperty(child)
			def separator= properties==""?"":"\n"
			properties="${properties}${separator}${property}"
		}
		return properties
	}
	
	def parseSingleProperty(mvnProperty){
		def tagCamel=CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, mvnProperty.name())
		return "${tagCamel} = ${mvnProperty.text()}"
	}
}