package migration.versiones

import java.io.File;

class MigratorVersiones{

	private File projectDir
	VersionesParser dependenciesParser

	MigratorVersiones(projectDir){
		this.projectDir = projectDir
		dependenciesParser=new VersionesParser()
	}

	void migrate(String inputFile,String outputFile){
		dependenciesParser.parseFile(
			new File(projectDir,inputFile),new File(projectDir,outputFile))
	}

}
