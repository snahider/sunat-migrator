package migration.repository

import org.apache.commons.io.FileUtils

class GitIgnore {
	
	def copyTemplate(srcDir,dstDir){
		def gitignore=new File(srcDir,".gitignore")
		FileUtils.copyFile(srcDir, dstDir);
	}
}
