package migration.repository

class NoParentBuildException extends Exception {
	NoParentBuildException(projectName){
		super("No se encontró el parent build.gradle $projectName")
	}
}
