package migration.repository

import java.io.File;

import org.tmatesoft.svn.core.wc.SVNClientManager
import org.tmatesoft.svn.core.wc.SVNCommitClient
import org.tmatesoft.svn.core.wc.SVNUpdateClient
import org.tmatesoft.svn.core.wc.SVNRevision
import org.tmatesoft.svn.core.SVNDepth
import org.tmatesoft.svn.core.SVNURL
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions

class SvnRepository{
	
	SVNClientManager clientManager
	
	SvnRepository(){
		clientManager = SVNClientManager.newInstance();
	}
	
	SvnRepository(user,pass){
		clientManager = SVNClientManager.newInstance(new DefaultSVNOptions(), user, pass);
	}
	
	void checkout(url,dest){
		File dstPath = new File(dest);   
		SVNURL svnUrl = SVNURL.parseURIEncoded(url); 
		SVNUpdateClient updateClient = clientManager.getUpdateClient();   
		updateClient.doCheckout(svnUrl, dstPath, SVNRevision.UNDEFINED, SVNRevision.HEAD, SVNDepth.INFINITY,false);
	}
	
	void importFiles(url,file){
		SVNURL svnUrl = SVNURL.parseURIEncoded(url);  
        SVNCommitClient commitClient=clientManager.getCommitClient();
		commitClient.doImport(file, SVNURL.parseURIEncoded(url), "import files", null, false, true, SVNDepth.fromRecurse(true));
	}
	
	void export(String url,long revision,File dest){
		SVNURL svnUrl = SVNURL.parseURIEncoded(url);
		SVNUpdateClient updateClient = clientManager.getUpdateClient();
		updateClient.doExport(svnUrl, dest,
			SVNRevision.create(revision),SVNRevision.create(revision),
			null, false, SVNDepth.EMPTY);
	}
}
