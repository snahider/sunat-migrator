package migration.repository

class CleanProjectFiles{
	void clean(projectDir){
		projectDir.eachFile{ File file ->
			if(file.isFile()){
				file.delete()
			}
			if(file.isDirectory() && file.name!=".git"){
				file.deleteDir()
			}
		}
	}
}
