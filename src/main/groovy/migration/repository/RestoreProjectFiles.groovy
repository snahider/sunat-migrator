package migration.repository

import org.apache.commons.io.FileUtils
import static groovy.io.FileType.*

class RestoreProjectFiles{
	void restore(projectDir,projectBackupDir){
		restoreGitIgnore(projectDir,projectBackupDir)
		restoreAllGradleFiles(projectDir,projectBackupDir)
		//restoreDotGit(projectDir,projectBackupDir)
	}

	private restoreGitIgnore(projectDir,projectBackupDir){
		def gitIgnore=new File(projectBackupDir,".gitignore")
		if(gitIgnore.exists())
			FileUtils.copyFileToDirectory(gitIgnore, projectDir)
	}

	private restoreAllGradleFiles(projectDir,projectBackupDir){
		projectBackupDir.eachFileRecurse(FILES) { File file ->
			def ext = file.name.substring(file.name.lastIndexOf('.')+1, file.name.length());
			if(ext=="gradle"){
				if(file.parentFile.name==projectDir.name){
					FileUtils.copyFileToDirectory(file, projectDir)
				}
				else{
					def subDir=new File(projectDir,file.parentFile.name)
					FileUtils.copyFileToDirectory(file, subDir)
				}
			}
		}
	}

	private restoreDotGit(projectDir,projectBackupDir){
		def git=new File(projectBackupDir,'.git')
		if(git.exists())
			FileUtils.moveDirectoryToDirectory(git, projectDir,false)
	}
}
