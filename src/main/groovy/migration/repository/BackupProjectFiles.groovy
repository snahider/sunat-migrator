package migration.repository

import org.apache.commons.io.FileUtils
import static groovy.io.FileType.*

class BackupProjectFiles{
	void backup(projectDir,projectBackupDir){
		//backupGitDirectory(projectDir,projectBackupDir)
		backupGitIgnore(projectDir,projectBackupDir)
		backupAllGradleFiles(projectDir,projectBackupDir)
	}
	
	private backupGitDirectory(projectDir,projectBackupDir){
		def git=new File(projectDir,'.git')
		if(git.exists())
			FileUtils.moveDirectoryToDirectory(git, projectBackupDir,false)
	}
	
	private backupGitIgnore(projectDir,projectBackupDir){
		def gitIgnore=new File(projectDir,".gitignore")
		if(gitIgnore.exists())
			FileUtils.copyFileToDirectory(gitIgnore, projectBackupDir)
	}

	private backupAllGradleFiles(projectDir,projectBackupDir){
		projectDir.eachFileRecurse(FILES) { File file ->
			def ext = file.name.substring(file.name.lastIndexOf('.')+1, file.name.length());
			if(ext=="gradle"){
				if(file.parentFile.name==projectDir.name){
					FileUtils.copyFileToDirectory(file, projectBackupDir)
				}
				else{
					def subDir=new File(projectBackupDir,file.parentFile.name)
					subDir.mkdir()
					FileUtils.copyFileToDirectory(file, subDir)
				}
			}
		}
	}
}
