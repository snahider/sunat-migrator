package migration.repository

import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.bean.CsvToBean
import au.com.bytecode.opencsv.bean.HeaderColumnNameMappingStrategy
import java.util.List;

class ProjectCsvReader {
	List<Project> readProjects(projectsFile){
		CSVReader reader = null;
		try {
			InputStream input = new FileInputStream(projectsFile);
			reader = new CSVReader(new InputStreamReader(input));
			HeaderColumnNameMappingStrategy<Project> strategy = new HeaderColumnNameMappingStrategy<Project>();
			strategy.setType(Project.class);
			CsvToBean<Project> csv = new CsvToBean<Project>();
			List<Project> orders = csv.parse(strategy, reader);
			return orders;
		}catch(e){
			println "No se pudo leer el archivo $projectsFile.path"
		}finally {
			if (reader != null) {
				reader.close();
			}
		}
	}
}
