package migration.repository

import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.bean.CsvToBean
import au.com.bytecode.opencsv.bean.HeaderColumnNameMappingStrategy
import org.apache.commons.io.FileUtils

class MigratorUpdateGitRepository {

	def workingDir
	def gitRepository
	def svnRepository
	def csvReader

	MigratorUpdateGitRepository(workingDir,svnRepository,gitRepository,csvReader){
		this.workingDir=workingDir
		this.svnRepository=svnRepository
		this.gitRepository=gitRepository
		this.csvReader=csvReader
	}

	void migrate(projecName,backupDir,svnUrl,gitUrl){
		def projectDir=new File(workingDir,projecName)
		projectDir.mkdir()
		gitRepository.clone(gitUrl, projectDir.path)
		validateBuildGradle(projectDir)
		def projectBackupDir=createProjectBackupDir(projectDir,backupDir)
		new BackupProjectFiles().backup(projectDir,projectBackupDir)
		new CleanProjectFiles().clean(projectDir)
		svnRepository.checkout(svnUrl, projectDir.path)
		new RestoreProjectFiles().restore(projectDir,projectBackupDir)
		deleteOldSvnFiles(projectDir)
		gitRepository.commitAndPush();
	}
	
	void migrate(file){
		if(!file.exists())
			throw new Exception("No se ha encontrado el archivo $file.path")
		
		def projects=csvReader.readProjects(file)
		for (project in projects) {
			println "Migrando proyecto $project.name"
			try{
				def backupDir= new File(workingDir,"backup")
				backupDir.mkdir()
				migrate(project.name,backupDir,project.svnUrl,project.gitUrl)
			}
			catch(e){
				println "Error migrando el proyecto $project.name"
				e.printStackTrace()
			}
		}
	}
	
	private validateBuildGradle(projectDir){
		def parentBuild=new File(projectDir.path,"build.gradle")
		if(!parentBuild.exists())
			throw new NoParentBuildException()
	}
	
	private createProjectBackupDir(projectDir,backupDir){
		def projectBackupDir=new File(backupDir,projectDir.name)
		projectBackupDir.mkdir()
		return projectBackupDir
	}
	
	private deleteOldSvnFiles(projectDir){
		def svn=new File(projectDir,".svn")
		if(svn.exists())
			svn.deleteDir()
	}
}