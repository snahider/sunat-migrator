package migration.repository

import au.com.bytecode.opencsv.CSVReader
import au.com.bytecode.opencsv.bean.CsvToBean
import au.com.bytecode.opencsv.bean.HeaderColumnNameMappingStrategy
import org.apache.commons.io.FileUtils

class MigratorCreateGitRepository {

	def workingDir
	def appHome
	def gitRepository
	def svnRepository
	def csvReader

	MigratorCreateGitRepository(workingDir,appHome,svnRepository,gitRepository,csvReader){
		this.workingDir=workingDir
		this.appHome=appHome
		this.svnRepository=svnRepository
		this.gitRepository=gitRepository
		this.csvReader=csvReader
	}

	void migrate(projecName,svnUrl,gitUrl){
		def projectDir=new File(workingDir,projecName)
		projectDir.mkdir()
		gitRepository.clone(gitUrl, projectDir.path)
		copyGitIgnore(projectDir)
		svnRepository.checkout(svnUrl,  projectDir.path)
		gitRepository.commitAndPush();
	}

	void migrate(file){
		if(!file.exists())
			throw new Exception("No se ha encontrado el archivo $file.path")
		
		def projects=csvReader.readProjects(file)
		for (project in projects) {
			println "Migrando proyecto $project.name"
			try{
				migrate(project.name,project.svnUrl,project.gitUrl)
			}
			catch(e){
				println "Error migrando el proyecto $project.name"
				e.printStackTrace()
			}
		}
	}

	private void copyGitIgnore(projectDir){
		FileUtils.copyFile(new File(appHome,"arquetipo/gitignore.txt"), new File(projectDir,".gitignore"));
	}
}