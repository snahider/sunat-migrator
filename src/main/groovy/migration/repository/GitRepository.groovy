package migration.repository

import org.ajoberstar.grgit.Credentials
import org.ajoberstar.grgit.Grgit
import org.ajoberstar.grgit.Grgit.*
import org.eclipse.jgit.api.*

class GitRepository {
	
	def grgit;
	def credentials=null
	
	GitRepository(){}
	
	GitRepository(user,pass){
		this.credentials=new Credentials(username: user, password: pass)
	}
	
	
	void init(path){
		grgit= Grgit.init(dir: path)
	}
	
	void clone(url,downloadDir){
		this.grgit=Grgit.clone(
				dir: downloadDir,
				uri: url,
				checkout:true,
				credentials:credentials
			)
	}
	
	void commit(){
		grgit.add(patterns: ["."])
		grgit.add(patterns: ["."],update: true)
		grgit.commit(message: 'Update Repository')
	}
	
	void push(){
		grgit.push(remote: 'origin')
	}
	
	void commitAndPush(){
		try{
			commit()
			push()
		}catch(e){
			throw e
		}finally{
			close()
		}
	}
	
	void close(){
		if(grgit!=null) grgit.close()
	}
}
