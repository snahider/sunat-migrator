package migration
import migration.dependencies.MigratorDependencies
import migration.geco.CFGDownloader
import migration.geco.DirectoryTree
import migration.geco.MigratorDownloadCfg;
import migration.repository.GitRepository
import migration.repository.MigratorCreateGitRepository
import migration.repository.MigratorUpdateGitRepository
import migration.repository.ProjectCsvReader
import migration.repository.SvnRepository
import migration.script.MigratorBuild
import migration.versiones.MigratorVersiones

import org.kohsuke.args4j.CmdLineParser

class MainApplication {

	def workingDir
	def appHome

	MainApplication(File workingDir,File appHome){
		this.workingDir=workingDir
		this.appHome=appHome
	}

	static void main(String[] args) {
		def workingDir=new File('.')
		def appHome=new File(System.getenv()['APP_HOME'])
		MainApplication mainApplication=new MainApplication(workingDir,appHome)
		mainApplication.run(args)
	}

	void run(args){

		ConsoleParams consoleParams=new ConsoleParams()
		CmdLineParser parser = new CmdLineParser(consoleParams);
		parser.parseArgument(args);

		if(consoleParams.isScript()){
			MigratorBuild migrator=new MigratorBuild(this.workingDir,this.appHome)
			migrator.migrate()
		}else if(consoleParams.isCreateGitRepository()){
			println "InputFile: $consoleParams.inputFile"
			MigratorCreateGitRepository migrator=new MigratorCreateGitRepository(
					this.workingDir,
					this.appHome,
				 	new SvnRepository(consoleParams.svnUser,consoleParams.svnPassword),
					new GitRepository(consoleParams.gitUser,consoleParams.gitPassword),
					new ProjectCsvReader())
			migrator.migrate(new File(consoleParams.inputFile))
		}else if(consoleParams.isUpdateGitRepository()){
			println "InputFile: $consoleParams.inputFile"
			MigratorUpdateGitRepository migrator=new MigratorUpdateGitRepository(
					this.workingDir,
					new SvnRepository(consoleParams.svnUser,consoleParams.svnPassword),
					new GitRepository(consoleParams.gitUser,consoleParams.gitPassword),
					new ProjectCsvReader())
			migrator.migrate(new File(consoleParams.inputFile))
		}else if(consoleParams.isDependencies()){
			MigratorDependencies migrator=new MigratorDependencies(this.workingDir)
			migrator.migrate(consoleParams.dependencyInput,consoleParams.dependencyOutput)
		}else if(consoleParams.isVersiones()){
			MigratorVersiones migrator=new MigratorVersiones(this.workingDir)
			migrator.migrate(consoleParams.dependencyInput,consoleParams.dependencyOutput)
		}else if(consoleParams.isDownloadCfg()){
			CFGDownloader cfgDownloader=new CFGDownloader(
				new SvnRepository(consoleParams.svnUser,consoleParams.svnPassword),
				new DirectoryTree(consoleParams.svnUrl))
			MigratorDownloadCfg migrator= new MigratorDownloadCfg(this.workingDir,cfgDownloader)
			migrator.downloadAllProyects(new File(consoleParams.cfgsDir))
		}else{
			throw new CommandNotFoundException()
		}
	}
}

