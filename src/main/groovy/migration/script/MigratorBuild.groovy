package migration.script

import migration.dependencies.DependenciesParser

class MigratorBuild{

	private File parentProjectDir
	private File appHome
	private File scriptsDir
	DependenciesParser dependenciesParser

	ArrayList<File> validSubProjects=['jar', 'war', 'ear']

	MigratorBuild(parentProjectDir,appHome){
		this.parentProjectDir = parentProjectDir
		this.appHome=appHome
		dependenciesParser=new DependenciesParser()
	}

	void migrate(){
		def subProjectsDirs=getSubProjectsDirs(parentProjectDir)
		def settingsFile=new SettingsFile()
		settingsFile.create(parentProjectDir,subProjectsDirs)
		def buildScriptFiles=new BuildScriptFiles(parentProjectDir,new File(appHome,"arquetipo"))
		buildScriptFiles.copyBuildScripts(parentProjectDir,subProjectsDirs)
		for(subProjectDir in subProjectsDirs){
			if(subProjectDir.path.endsWith("jar")){
				File pom=new File(subProjectDir,'pom.xml')
				File gradle=new File(subProjectDir,'build.gradle')
				dependenciesParser.parseFile(pom,gradle)
			}
		}
	}

	def getSubProjectsDirs(parentProjectDir){
		def subDirs=[]
		parentProjectDir.eachDir() { dir ->
			boolean validSubProject=validSubProjects.any { validDir->
				dir.name.endsWith(validDir)
			}
			if(validSubProject){
				subDirs << dir
			}
		}
		return subDirs
	}
}
