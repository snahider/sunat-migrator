package migration.script

class SettingsFile{
	void create(def parentProjectDir, def subProjectsDirs){
		String content="include"
		for (subProject in subProjectsDirs) {
			String projectName = content=="include"?" '$subProject.name'":", '$subProject.name'"
			content = content + projectName
		}
		File settings=new File(parentProjectDir,'settings.gradle')
		settings << content
	}
}
