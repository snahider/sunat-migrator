package migration.script

import java.io.File;
import org.apache.commons.io.FileUtils

class BuildScriptFiles{
	private File parentProjectDir
	private File scriptsDir

	BuildScriptFiles(parentProjectDir,scriptsDir){
		this.parentProjectDir = parentProjectDir
		this.scriptsDir = scriptsDir
	}

	void copyBuildScripts(parentProjectDir,subProjectsDirs){
		File build=new File(scriptsDir,"build.gradle")
		FileUtils.copyFile(build, new File(parentProjectDir,"build.gradle"));
		for (subProjectDir in subProjectsDirs) {
			build=getSubProjectBuild(subProjectDir)
			FileUtils.copyFile(build, new File(subProjectDir,"build.gradle"));
		}
	}

	File getSubProjectBuild(File subProjectDir){
		if(subProjectDir.path.endsWith("ear")){
			return new File(scriptsDir,"ear/build.gradle")
		}
		if(subProjectDir.path.endsWith("war")){
			return new File(scriptsDir,"war/build.gradle")
		}
		if(subProjectDir.path.endsWith("jar")){
			return new File(scriptsDir,"jar/build.gradle")
		}
		throw new Exception("Invalid SubProject Directory Name: $subProjectDir.name")
	}
}