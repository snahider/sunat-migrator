package migration.dependencies

import java.io.File;

class MigratorDependencies{

	private File projectDir
	DependenciesParser dependenciesParser

	MigratorDependencies(projectDir){
		this.projectDir = projectDir
		dependenciesParser=new DependenciesParser()
	}

	void migrate(String inputFile,String outputFile){
		dependenciesParser.parseFile(
			new File(projectDir,inputFile),new File(projectDir,outputFile))
	}

}
