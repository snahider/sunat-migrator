package migration.dependencies

import com.google.common.base.CaseFormat

class DependenciesParser{

	void parseFile(File inputFile,File outputFile){
		def project = new XmlParser().parse(inputFile)
		def text=parse(project);
		outputFile << text
	}

	String parseText(String dependenciesMvn){
		def project = new XmlParser().parseText(dependenciesMvn)
		return parse(project);
	}

	String parse(project){
		def dependencies=""
		if(project.dependencies.size()>0){
			dependencies="dependencies{\n}";
			if(project.dependencies.dependency.size()>0){
				def dependenciesList=parseDependenciesList(project.dependencies)
				dependencies="dependencies{\n\t${dependenciesList}\n}"
			}
		}
		return dependencies;
	}
	
	String parseDependenciesList(dependencies){
		def dependenciesList=''
		dependencies.dependency.each { dep ->
			def group=parseGroup(dep)
			def name="name: '${dep.artifactId.text()}'"
			def version=parseVersion(dep)
			def scope=parseScope(dep)
			
			def dependency="${scope} ${group}, ${name}, ${version}"
			if(dep.exclusions){
				def exclusionsList=parseExclusionList(dep.exclusions)
				dependency="${scope}(${group}, ${name}, ${version}){\n\t\t${exclusionsList}\n\t}"
			}
			def separator=dependenciesList==""?"":"\n\t"
			dependenciesList="${dependenciesList}${separator}${dependency}"
		}
		return dependenciesList
	}
	
	String parseGroup(dep){
		def groupName="'${dep.groupId.text()}'"
		if(groupName.startsWith("'\${grp-") && groupName.endsWith("}'")){
			def groupCapitalize=getGroupCapitalize(groupName)
			groupName="versiones.grp${groupCapitalize}"
		}
		return "group: ${groupName}"
	}
	
	String parseVersion(dep){
		def versionNumber="'+'"
		def groupName="'${dep.groupId.text()}'"
		if(groupName.startsWith("'\${grp-") && groupName.endsWith("}'")){
			def groupCapitalize=getGroupCapitalize(groupName)
			versionNumber="versiones.vrs${groupCapitalize}"
		}else if(dep.version){
			versionNumber="'${dep.version.text()}'"
		}
		return "version: ${versionNumber}"
	}
	
	String parseScope(dep){
		def scope="compile"
		if(dep.scope && dep.scope.text()=="test"){
			scope="testCompile"
		}
		return scope;
	}
	
	String parseExclusionList(exclusions){
		def exclusionsList=''
		exclusions.exclusion.each{ exc ->
			def exclusion=parseExclusion(exc)
			def separator=exclusionsList==""?"":"\n\t\t"
			exclusionsList="${exclusionsList}${separator}${exclusion}"
		}
		return exclusionsList
	}
	
	String parseExclusion(exclusion){
		def exclusionGroup=parseGroup(exclusion)
		def exclusionModule="module: '${exclusion.artifactId.text()}'"
		return "exclude ${exclusionGroup}, ${exclusionModule}"
	}
	
	String getGroupCapitalize(String groupName){
		def groupId=groupName.substring(7, groupName.size()-2)
		def groupCamel=CaseFormat.LOWER_HYPHEN.to(CaseFormat.LOWER_CAMEL, groupId)
		return groupCamel.substring(0, 1).toUpperCase() + groupCamel.substring(1)
	}
}
