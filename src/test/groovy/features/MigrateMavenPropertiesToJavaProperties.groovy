package features

import static org.hamcrest.CoreMatchers.instanceOf
import static org.junit.Assert.*
import migration.MainApplication

import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

public class MigrateMavenPropertiesToJavaProperties {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	File projectDir
	File appHome
	
	@Before
	public void setup(){
		projectDir=tempFolder.newFolder("proyecto")
	}

	@Test
	public void parseDependencies(){
		def versionesInput=new File(projectDir,"versiones-input.xml")
		String fileInput='''
		<project><properties>
			<jasper-version>3.7.3</jasper-version>
			<grp-administracion2-tramite-sharedlib>procesos.administracion2.tramite.administracion2-tramite-sharedlib</grp-administracion2-tramite-sharedlib>
			<vrs-administracion2-tramite-sharedlib>2.0.1</vrs-administracion2-tramite-sharedlib>
		</properties></project>'''
		versionesInput << fileInput
		
		String fileExpected='''jasperVersion = 3.7.3
grpAdministracion2TramiteSharedlib = procesos.administracion2.tramite.administracion2-tramite-sharedlib
vrsAdministracion2TramiteSharedlib = 2.0.1'''
		
		String command="versiones -depIn versiones-input.xml -depOut versiones-actual.txt"
		MainApplication main =new MainApplication(projectDir,appHome)
		main.run(command.split("\\s+"))
		
		File fileActual=new File(projectDir,"versiones-actual.txt")
		assertEquals(fileExpected,fileActual.text)
	}
}