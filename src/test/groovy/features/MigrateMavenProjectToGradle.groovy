package features

import static org.hamcrest.CoreMatchers.instanceOf
import static org.junit.Assert.*
import migration.MainApplication

import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

public class MigrateMavenProjectToGradle {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	File projectDir
	File appHome
	
	@Before
	public void setup(){
		projectDir=tempFolder.newFolder("proyecto")
		copyTestResourcesToProjectDir(projectDir)
		appHome=getAppHome()
	}
	
	@Test
	public void copyGradleFiles(){

		String command="script"
		MainApplication main =new MainApplication(projectDir,appHome)
		main.run(command.split("\\s+"))

		File jarBuild=new File(projectDir,"proyecto.jar/build.gradle")
		assertTrue(jarBuild.exists())
		File warBuild=new File(projectDir,"proyecto.war/build.gradle")
		assertTrue(warBuild.exists())
		File earBuild=new File(projectDir,"proyecto.ear/build.gradle")
		assertTrue(earBuild.exists())
		File parentBuild =new File(projectDir,"build.gradle")
		assertTrue(parentBuild.exists())
		File settingsFile =new File(projectDir,"settings.gradle")
		assertTrue(settingsFile.exists())
	}

	@Test
	public void parseDependencies(){
		String command="script"
		MainApplication main =new MainApplication(projectDir,appHome)
		main.run(command.split("\\s+"))
		
		File actual=new File(projectDir,"proyecto.jar/build.gradle")
		ClassLoader classLoader = getClass().getClassLoader();
		File expected=new File(classLoader.getResource("migration/expected-jar-build.txt").getFile())
		assertEquals(expected.text,actual.text)
	}
	
	
	def copyTestResourcesToProjectDir(projectDir){
		ClassLoader classLoader = getClass().getClassLoader()
		String projectSource=classLoader.getResource("migration/proyecto").getFile()
		FileUtils.copyDirectory(new File(projectSource), projectDir);
	}

	def getAppHome(){
		File appHome=System.getenv("APP_HOME")
		if(!appHome){
			String currentDir = System.getProperty("user.dir")
			appHome=new File("$currentDir/src/dist")
		}
		return appHome
	}
}