package features

import static org.junit.Assert.*
import migration.MainApplication
import migration.repository.GitRepository
import migration.repository.SvnRepository

import org.ajoberstar.grgit.Grgit
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.tmatesoft.svn.core.SVNURL
import org.tmatesoft.svn.core.io.SVNRepositoryFactory

public class CreateGitRepositoryFromSvnTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	File svnRepo
	File gitRepo
	File appHome
	File assertsDir
	SVNURL svnUrl
	def git

	@Before
	public void setup() {
		appHome=tempFolder.newFolder('appHome')
		svnRepo=tempFolder.newFolder("svn-repo.svn")
		gitRepo=tempFolder.newFolder("git-repo.git")
		assertsDir=tempFolder.newFolder("assertsDir")
		svnUrl=initSvnRepository()
		git = initGitRepository()
	}


	@Test
	public void migrateSvnToGit(){
		createGitIgnoreInTemplatesDir()
		def csv=createCsvWithProjects()

		String command="createRepository -svnUser user -svnPass pass -gitUser user -gitPass pass -inputFile $csv.path"
		MainApplication main =new MainApplication(tempFolder.folder,appHome)
		main.run(command.split("\\s+"))

		cloneFilesToAssert()
		assertAllFilesPushed()
		assertGitIgnoreIsPresent()
	}

	def createGitIgnoreInTemplatesDir(){
		File templates=new File(appHome,'arquetipo')
		templates.mkdir()
		new File(templates,'gitignore.txt').createNewFile()
	}

	def createCsvWithProjects(){
		File csv=tempFolder.newFile()
		csv<<"name,svnUrl,gitUrl\n"
		csv<<"project1,file:///$svnUrl.path,$gitRepo.path"
		return csv
	}

	def initSvnRepository(){
		SVNURL svnUrl = SVNRepositoryFactory.createLocalRepository(svnRepo, true , false)
		ClassLoader classLoader = getClass().getClassLoader();
		File projectDir=new File(classLoader.getResource("svnrepo").getFile())
		SvnRepository svnRepository=new SvnRepository()
		svnRepository.importFiles("file:///$svnUrl.path", projectDir)
		return svnUrl
	}

	def initGitRepository(){
		return Grgit.init(dir: gitRepo.path)
	}

	def cloneFilesToAssert(){
		GitRepository gitRepository=new GitRepository()
		gitRepository.clone(gitRepo.path, assertsDir.path)
		gitRepository.close()
	}

	def assertAllFilesPushed(){
		File file1=new File(assertsDir,"file1.txt")
		assertTrue(file1.exists())
	}

	def assertGitIgnoreIsPresent(){
		File gitignore=new File(assertsDir,".gitignore")
		assertTrue(gitignore.exists())
	}
}