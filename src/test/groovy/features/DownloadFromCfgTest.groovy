package features

import static org.junit.Assert.*
import migration.MainApplication
import migration.repository.GitRepository
import migration.repository.SvnRepository

import org.ajoberstar.grgit.Grgit
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.tmatesoft.svn.core.SVNURL
import org.tmatesoft.svn.core.io.SVNRepositoryFactory
import org.eclipse.jgit.api.Git 

public class DownloadFromCfgTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	File appHome
	File workingDir
	File cfgDir

	@Before
	public void setup() {
		appHome=tempFolder.newFolder('appHome')
		workingDir=tempFolder.newFolder('workingDir')
		cfgDir=tempFolder.newFolder('cfgDir')
	}

	@Test
	public void downloadProjectsFromCfg(){
		def svnUrl=initSvnRepository()
		createCfgsInCfgDir(svnUrl)

		String command="downloadCfg -svnUrl file://$svnUrl.path -svnUser user -svnPass pass -cfgsDir $cfgDir.path"
		MainApplication main =new MainApplication(workingDir,appHome)
		main.run(command.split("\\s+"))

		assertAllFilesDownloaded()
	}

	def createCfgsInCfgDir(svnUrl){
		File cfg1=new File(cfgDir,"proj1.ear.cfg")
		cfg1<< "file://$svnUrl.path/proj1/dir1/file1.txt|1|0|\n"
		cfg1<< "file://$svnUrl.path/proj1/dir2/file2.txt|1|0|"
		
		File cfg2=new File(cfgDir,"proj2.ear.cfg")
		cfg2<< "file://$svnUrl.path/proj2/dir1/file1.txt|1|0|\n"
		cfg2<< "file://$svnUrl.path/proj2/dir2/file2.txt|1|0|"
	}

	
	def initSvnRepository(){
		def dirToImport=createFilesToImport()
		def svnRepo=tempFolder.newFolder("svn-repo.svn")
		SVNURL svnUrl = SVNRepositoryFactory.createLocalRepository(svnRepo, true , false)
		SvnRepository svnRepository=new SvnRepository()
		svnRepository.importFiles("file:///$svnUrl.path", dirToImport)
		return svnUrl
	}
	
	def createFilesToImport(){
		def dirToImport=tempFolder.newFolder("dirToImport")
		
		def proj1=new File(dirToImport,"proj1")
		proj1.mkdir()
		def dir1=new File(proj1,"dir1")
		dir1.mkdir()
		new File(dir1,"file1.txt").createNewFile()
		def dir2=new File(proj1,"dir2")
		dir2.mkdir()
		new File(dir2,"file2.txt").createNewFile()
		
		def proj2=new File(dirToImport,"proj2")
		proj2.mkdir()
		dir1=new File(proj2,"dir1")
		dir1.mkdir()
		new File(dir1,"file1.txt").createNewFile()
		dir2=new File(proj2,"dir2")
		dir2.mkdir()
		new File(dir2,"file2.txt").createNewFile()
		
		return dirToImport
	}

	def assertAllFilesDownloaded(){
		
		def proj1Ear=new File(workingDir,"proj1.ear")
		def proj1=new File(proj1Ear,"proj1")
		def file1Exits=new File(proj1,"dir1/file1.txt").exists()
		assertTrue(file1Exits)
		def file2Exits=new File(proj1,"dir2/file2.txt").exists()
		assertTrue(file2Exits)
		
		def proj2Ear=new File(workingDir,"proj2.ear")
		def proj2=new File(proj2Ear,"proj2")
		file1Exits=new File(proj2,"dir1/file1.txt").exists()
		assertTrue(file1Exits)
		file2Exits=new File(proj2,"dir2/file2.txt").exists()
		assertTrue(file2Exits)
	}
}