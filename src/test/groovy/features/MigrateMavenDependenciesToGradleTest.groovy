package features

import static org.hamcrest.CoreMatchers.instanceOf
import static org.junit.Assert.*
import migration.MainApplication

import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

public class MigrateMavenDependenciesToGradleTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	File projectDir
	File appHome
	
	@Before
	public void setup(){
		projectDir=tempFolder.newFolder("proyecto")
		copyInputToProjectDir(projectDir)
		appHome=getAppHome()
	}

	@Test
	public void parseDependencies(){
		String command="dependencias -depIn dependencies-input.xml -depOut dependencies-actual.txt"
		MainApplication main =new MainApplication(projectDir,appHome)
		main.run(command.split("\\s+"))
		
		File actual=new File(projectDir,"dependencies-actual.txt")
		ClassLoader classLoader = getClass().getClassLoader();
		File expected=new File(classLoader.getResource("migration/dependencies/dependencies-expected.txt").getFile())
		assertEquals(expected.text,actual.text)
	}
	
	
	def copyInputToProjectDir(projectDir){
		ClassLoader classLoader = getClass().getClassLoader()
		String inputFile=classLoader.getResource("migration/dependencies/dependencies-input.xml").getFile()
		FileUtils.copyFile(new File(inputFile), new File(projectDir,"dependencies-input.xml"))
	}

	def getAppHome(){
		File appHome=System.getenv("APP_HOME")
		if(!appHome){
			String currentDir = System.getProperty("user.dir")
			appHome=new File("$currentDir/src/dist")
		}
		return appHome
	}
}