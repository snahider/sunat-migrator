package features

import static org.junit.Assert.*
import migration.MainApplication
import migration.repository.GitRepository
import migration.repository.SvnRepository

import org.ajoberstar.grgit.Grgit
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.tmatesoft.svn.core.SVNURL
import org.tmatesoft.svn.core.io.SVNRepositoryFactory
import org.eclipse.jgit.api.Git 

public class UpdateGitRepositoryFromSvnTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	File svnRepo
	File gitRepo
	File appHome
	File assertsDir

	@Before
	public void setup() {
		appHome=tempFolder.newFolder('appHome')
		svnRepo=tempFolder.newFolder("svn-repo.svn")
		gitRepo=tempFolder.newFolder("git-repo.git")
		assertsDir=tempFolder.newFolder("assertsDir")
		initSvnRepository()
		initGitRepository()
	}


	@Test
	public void updateGitRepositoryFromSvn(){
		def csv=createCsvWithProjects()

		String command="updateRepository -svnUser user -svnPass pass -gitUser user -gitPass pass -inputFile $csv.path"
		MainApplication main =new MainApplication(tempFolder.folder,appHome)
		main.run(command.split("\\s+"))

		cloneFilesToAssert()
		assertAllFilesPushed()
	}

	def createCsvWithProjects(){
		File csv=tempFolder.newFile()
		csv<<"name,svnUrl,gitUrl\n"
		csv<<"project1,file:///$svnRepo.path,$gitRepo.path"
		return csv
	}

	def initSvnRepository(){
		def projectDir=tempFolder.newFolder()
		
		def modeloDir=new File(projectDir,"modelo.jar")
		modeloDir.mkdir()
		def file1=new File(modeloDir,"file1.txt")
		file1.createNewFile()
		file1<<"new file"
		def file2=new File(modeloDir,"file2.txt")
		file2.createNewFile()
		def subDir=new File(modeloDir,"subDir")
		subDir.mkdir()
		def file3=new File(subDir,"file3.txt")
		file3.createNewFile()
		
		SVNURL svnUrl = SVNRepositoryFactory.createLocalRepository(svnRepo, true , false)
		SvnRepository svnRepository=new SvnRepository()
		svnRepository.importFiles("file:///$svnRepo.path", projectDir)
	}

	def initGitRepository(){
		GitRepository gitRepository=new GitRepository()
		gitRepository.init(gitRepo.path)

		def dotGit=new File(gitRepo,".git")
		dotGit.mkdir()
		def gitFile=new File(dotGit,".gitFile")
		gitFile.createNewFile()
		def buildParent=new File(gitRepo,"build.gradle")
		buildParent.createNewFile()
		def settings=new File(gitRepo,"settings.gradle")
		settings.createNewFile()
		def modeloDir=new File(gitRepo,"modelo.jar")
		modeloDir.mkdir()
		def buildSubproject=new File(modeloDir,"build.gradle")
		buildSubproject.createNewFile()
		def file1=new File(modeloDir,"file1.txt")
		file1.createNewFile()
		file1<<"file1"
		def dirToDelete=new File(modeloDir,"dirToDelete")
		dirToDelete.mkdir()
		def fileToDelete=new File(dirToDelete,"fileToDelete.txt")
		fileToDelete.createNewFile()
		
		gitRepository.commit()
	}

	def cloneFilesToAssert(){
		GitRepository gitRepository=new GitRepository()
		gitRepository.clone(gitRepo.path, assertsDir.path)
		gitRepository.close()
	}

	def assertAllFilesPushed(){
		
		def dotGit=new File(gitRepo,".git")
		def gitFile=new File(dotGit,".gitFile")
		def buildParent=new File(assertsDir,"build.gradle")
		def settings=new File(assertsDir,"settings.gradle")
		def modeloDir=new File(assertsDir,"modelo.jar")
		def buildSubproject=new File(modeloDir,"build.gradle")
		def file1=new File(modeloDir,"file1.txt")
		def file2=new File(modeloDir,"file2.txt")
		def subDir=new File(modeloDir,"subDir")
		def file3=new File(subDir,"file3.txt")
		def dirToDelete=new File(modeloDir,"dirToDelete")
		def fileToDelete=new File(dirToDelete,"fileToDelete.txt")
		
		assertTrue(gitFile.exists())
		assertTrue(file1.exists())
		assertTrue(file2.exists())
		assertTrue(file3.exists())
		assertTrue(buildParent.exists())
		assertTrue(settings.exists())
		assertTrue(buildSubproject.exists())
		assertEquals("new file",file1.text)
		assertFalse(dirToDelete.exists())
		assertFalse(fileToDelete.exists())
	}
}