package migration.geco

import java.io.File;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.junit.Assert.*
import groovy.io.FileType

import org.apache.commons.io.FilenameUtils

/*
 * TODO
 * - Solo los archivos cfg del directorio 
 * http://groovy-almanac.org/list-files-recursivly/
 * http://stackoverflow.com/questions/3662144/recursive-listing-of-all-files-matching-a-certain-filetype-in-groovy
 * 
 * - Ignorar algún error y continuar con el siguiente
 */

class MigratorDownloadCfgTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Test
	public void descargaLosProyectosDeTodosLosArchivosCFG(){
		def workingDir=tempFolder.newFolder()
		def cfgDownloader=new CFGDownloaderFake()
		def migratorGeco=new MigratorDownloadCfg(workingDir,cfgDownloader)
		
		def cfgsDir=tempFolder.newFolder()
		new File(cfgsDir,"cfgFile1.ear.cfg").createNewFile()
		new File(cfgsDir,"cfgFile2.ear.cfg").createNewFile()
		migratorGeco.downloadAllProyects(cfgsDir)
		
		def file1Exists=new File(workingDir,'cfgFile1.ear/file1.txt').exists()
		assertTrue(file1Exists)
		def file2Exists=new File(workingDir,'cfgFile2.ear/file2.txt').exists()
		assertTrue(file2Exists)
	}

	@Test
	public void excluyeArchivosNoCFG(){
		def workingDir=tempFolder.newFolder()
		def cfgDownloader=new CFGDownloaderFake()
		def migratorGeco=new MigratorDownloadCfg(workingDir,cfgDownloader)
		
		def cfgsDir=tempFolder.newFolder()
		new File(cfgsDir,"cfgFile1.ear.cfg").createNewFile()
		new File(cfgsDir,"excel.xls").createNewFile()
		migratorGeco.downloadAllProyects(cfgsDir)
		
		def excelExists=new File(workingDir,'excel').exists()
		assertFalse(excelExists)
	}
	
	@Test
	public void siElDirectorioYaExisteContinuaConElSiguienteCfg(){
		def workingDir=tempFolder.newFolder()
		def cfg1=new File(workingDir,"cfgFile1.ear")
		cfg1.mkdir()
		def oldFile=new File(cfg1,'oldFile.txt')
		oldFile.createNewFile()
		
		def cfgDownloader=new CFGDownloaderFake()
		def migratorGeco=new MigratorDownloadCfg(workingDir,cfgDownloader)
		
		def cfgsDir=tempFolder.newFolder()
		new File(cfgsDir,"cfgFile1.ear.cfg").createNewFile()
		new File(cfgsDir,"cfgFile2.ear.cfg").createNewFile()
		migratorGeco.downloadAllProyects(cfgsDir)
		
		def file1Exists=new File(workingDir,'cfgFile1.ear/file1.txt').exists()
		assertFalse(file1Exists)
		def oldFileExists= new File(workingDir,'cfgFile1.ear/oldFile.txt').exists()
		assertTrue(oldFileExists)
		def file2Exists=new File(workingDir,'cfgFile2.ear/file2.txt').exists()
		assertTrue(file2Exists)
	}
	
	@Test
	public void siSeProduceUnErrorContinuaConElSiguienteCfg(){
		ByteArrayOutputStream errContent = new ByteArrayOutputStream();
		System.setErr(new PrintStream(errContent));
		
		def workingDir=tempFolder.newFolder()
		def cfgDownloader=new CFGDownloaderFake()
		def migratorGeco=new MigratorDownloadCfg(workingDir,cfgDownloader)
		
		def cfgsDir=tempFolder.newFolder()
		new File(cfgsDir,"cfgFile1WithError.cfg").createNewFile()
		new File(cfgsDir,"cfgFile2.ear.cfg").createNewFile()
		migratorGeco.downloadAllProyects(cfgsDir)
		
		def file2Exists=new File(workingDir,'cfgFile2.ear/file2.txt').exists()
		assertTrue(file2Exists)
	}
	
	@After
	public void revertErrStream(){
		PrintStream stream =new PrintStream(new FileOutputStream(FileDescriptor.err))
		System.setErr(new PrintStream(stream));
	}
	
	class CFGDownloaderFake extends CFGDownloader{
		
		def CFGDownloaderFake(){
			super(null, null)
		}
		
		def downloadProyect(File destDir,File cfgFile){
			if(cfgFile.name=="cfgFile1.ear.cfg"){
				new File(destDir,'file1.txt').createNewFile()
			}
			if(cfgFile.name=="cfgFile2.ear.cfg"){
				new File(destDir,'file2.txt').createNewFile()
			}
			if(cfgFile.name=="cfgFile1WithError.cfg"){
				throw new Exception("Error descargando cfg")
			}
		}
	}
}
