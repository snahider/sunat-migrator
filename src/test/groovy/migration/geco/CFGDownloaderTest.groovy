package migration.geco

import static org.junit.Assert.*

import java.io.File;
import java.util.List;

import migration.repository.SvnRepository
import org.junit.After
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test
import org.junit.rules.TemporaryFolder;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.SVNURL
import org.tmatesoft.svn.core.io.SVNRepositoryFactory


class CFGDownloaderTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Test
	public void descargaTodosLosArchivosDelCFG(){
		def createDirToImport={
			def dirToImport=tempFolder.newFolder("dirToImport")
			def dir1=new File(dirToImport,"dir1")
			dir1.mkdir()
			new File(dir1,"file1.txt").createNewFile()
			new File(dir1,"file2.txt").createNewFile()
			def dir2=new File(dirToImport,"dir2")
			dir2.mkdir()
			new File(dir2,"file3.txt").createNewFile()
			new File(dir2,"build/classes").mkdirs()
			return dirToImport
		}
		def svnUrl=initSvnRepository(createDirToImport)
		
		def directoryTree=new DirectoryTree("file://$svnUrl.path/")
		def svnRepository=new SvnRepository();
		def cfgDownloader=new CFGDownloader(svnRepository,directoryTree)
		
		def destDir=tempFolder.newFolder()
		def cfg=tempFolder.newFile()
		cfg<< "file://$svnUrl.path/dir1/file1.txt|1|0|\n"
		cfg<< "file://$svnUrl.path/dir1/file2.txt|1|0|\n"
		cfg<< "file://$svnUrl.path/dir2/file3.txt|1|0|\n"
		cfg<< "file://$svnUrl.path/dir2/build/classes|1|0|"
		cfgDownloader.downloadProyect(destDir,cfg)
		
		def file1Exits=new File(destDir,"dir1/file1.txt").exists()
		assertTrue(file1Exits)
		def file2Exits=new File(destDir,"dir1/file2.txt").exists()
		assertTrue(file2Exits)
		def file3Exits=new File(destDir,"dir2/file3.txt").exists()
		assertTrue(file3Exits)
		def excludedExists=new File(destDir,"dir2/build/classes").exists()
		assertFalse(excludedExists)
	}
	
	@Test
	public void continuaLaDescargaSiNoExisteUnArchivoEnElSVN(){
		ByteArrayOutputStream errContent = new ByteArrayOutputStream();
		System.setErr(new PrintStream(errContent));
		
		def createDirToImport={
			def dirToImport=tempFolder.newFolder("dirToImport")
			def dir1=new File(dirToImport,"dir1")
			dir1.mkdir()
			new File(dir1,"file1.txt").createNewFile()
			new File(dir1,"file2.txt").createNewFile()
			return dirToImport
		}
		def svnUrl=initSvnRepository(createDirToImport)
		
		def directoryTree=new DirectoryTree("file://$svnUrl.path/")
		def svnRepository=new SvnRepository();
		def cfgDownloader=new CFGDownloader(svnRepository,directoryTree)
		
		def destDir=tempFolder.newFolder()
		def cfg=tempFolder.newFile()
		cfg<< "file://$svnUrl.path/dir1/file1.txt|1|0|\n"
		cfg<< "file://$svnUrl.path/dir1/fileThatNotExist.txt|1|0|\n"
		cfg<< "file://$svnUrl.path/dir1/file2.txt|1|0|\n"
		cfgDownloader.downloadProyect(destDir,cfg)
		
		def file1Exits=new File(destDir,"dir1/file1.txt").exists()
		assertTrue(file1Exits)
		def file2Exits=new File(destDir,"dir1/file2.txt").exists()
		assertTrue(file2Exits)
	}
	
	
	def initSvnRepository(createDirToImport){
		def dirToImport = createDirToImport()
		def svnRepo=tempFolder.newFolder("svn-repo.svn")
		SVNURL svnUrl = SVNRepositoryFactory.createLocalRepository(svnRepo, true , false)
		SvnRepository svnRepository=new SvnRepository()
		svnRepository.importFiles("file:///$svnUrl.path", dirToImport)
		return svnUrl
	}
	
	@After
	public void revertErrStream(){
		PrintStream stream =new PrintStream(new FileOutputStream(FileDescriptor.err))
		System.setErr(new PrintStream(stream));
	}
}
