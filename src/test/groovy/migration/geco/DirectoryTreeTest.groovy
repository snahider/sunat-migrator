package migration.geco

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.junit.Assert.*

class DirectoryTreeTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Test
	public void createDirectoryWithRepositoryHost(){
		def repositoryPath="http://localhost/"
		def directoryTree=new DirectoryTree(repositoryPath)
		
		def destDir=tempFolder.newFolder()
		def fullUrlPath="http://localhost/dir1/dir2/file1.txt"
		def directory=directoryTree.create(destDir,fullUrlPath)
		
		def directoryExpected=new File(destDir,"dir1/dir2")
		assertTrue(directory.exists())
		assertEquals(directoryExpected.path,directory.path)
	}
	
	@Test
	public void createDirectoryWithRepositoryPath(){
		def repositoryPath="http://localhost/repository/"
		def directoryTree=new DirectoryTree(repositoryPath)
		
		def destDir=tempFolder.newFolder()
		def fullUrlPath="http://localhost/repository/dir1/dir2/file1.txt"
		def directory=directoryTree.create(destDir,fullUrlPath)
		
		def directoryExpected=new File(destDir,"dir1/dir2")
		assertTrue(directory.exists())
		assertEquals(directoryExpected.path,directory.path)
	}

	@Test
	public void urlWithPortAndRepositoryHost(){
		def repositoryPath="http://localhost/"
		def directoryTree=new DirectoryTree(repositoryPath)
		
		def destDir=tempFolder.newFolder()
		def fullUrlPath="http://localhost:80/dir1/dir2/file1.txt"
		def directory=directoryTree.create(destDir,fullUrlPath)
		
		def directoryExpected=new File(destDir,"dir1/dir2")
		assertTrue(directory.exists())
		assertEquals(directoryExpected.path,directory.path)
	}
		
	@Test
	public void urlWithPortAndRepositoryPath(){
		def repositoryPath="http://localhost/repository/"
		def directoryTree=new DirectoryTree(repositoryPath)
		
		def destDir=tempFolder.newFolder()
		def fullUrlPath="http://localhost:80/repository/dir1/dir2/file1.txt"
		def directory=directoryTree.create(destDir,fullUrlPath)
		
		def directoryExpected=new File(destDir,"dir1/dir2")
		assertTrue(directory.exists())
		assertEquals(directoryExpected.path,directory.path)
	}
	
	@Test
	public void addEndingSlash(){
		def repositoryPath="http://localhost/repository"
		def directoryTree=new DirectoryTree(repositoryPath)
		
		assertEquals("http://localhost/repository/",directoryTree.repositoryPath)
	}
}
