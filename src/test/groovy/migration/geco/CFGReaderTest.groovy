package migration.geco

import static org.junit.Assert.*

import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder


class CFGReaderTest {

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	@Test
	public void leerLinea(){
		def cfgReader = new CFGReader()
		def line="http://localhost/repository/file1.txt|123|0|"
		
		def urlAndRevision=cfgReader.readLine(line)
		
		assertEquals("http://localhost/repository/file1.txt",urlAndRevision[0]);
		assertEquals(123,urlAndRevision[1]);
	}
	
	@Test
	public void readFile(){
		File gecoFile=createGfgFile()
		def cfgReader = new CFGReader()

		List cfg=cfgReader.readFile(gecoFile)
		
		assertEquals(2,cfg.size())
	}
	
	def createGfgFile(){
		File cfgFile=tempFolder.newFile()
		cfgFile<<"http://localhost/repository/dir1/file1.txt|123|0|\n"
		cfgFile<<"http://localhost/repository/dir2/file2.txt|1234|0|"
		return cfgFile
	}
}
