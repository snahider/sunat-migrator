package migration.repository

import static org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.tmatesoft.svn.core.SVNURL
import org.tmatesoft.svn.core.io.SVNRepositoryFactory

public class SvnRepositoryTest {
	
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	File svnRepo
	File svnCheckout
	
	@Before
	public void setup() {
		svnRepo=tempFolder.newFolder("svn-repo.svn")
		svnCheckout=tempFolder.newFolder("svn-repo")
	}
  
	@Test
	public void checkout_descargaLosDirectorios(){
	    SVNURL svnUrl = SVNRepositoryFactory.createLocalRepository(svnRepo, true , false )

	    SvnRepository svnDownoader=new SvnRepository()
	    svnDownoader.checkout("file:///$svnUrl.path",svnCheckout.path)

		File svn =new File(svnCheckout,'.svn')
		assertTrue(svn.exists())
	}
}