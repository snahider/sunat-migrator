package migration.repository

import static org.junit.Assert.*

import org.junit.After
import org.junit.Test

class MigratorCreateGitRepositoryTest {
	
	@After
	public void revertErrStream(){
		PrintStream stream =new PrintStream(new FileOutputStream(FileDescriptor.err))
		System.setErr(new PrintStream(stream));
	}
	
	@Test
	public void migrate_invalidFile_throwsException(){
		MigratorCreateGitRepository migrator=new MigratorCreateGitRepository(null,null,null,null,null)
		def file=new File('fileNotExist')
		try{
			migrator.migrate(file)
			fail()
		}catch(e){
			assertEquals("No se ha encontrado el archivo fileNotExist",e.message)
		}
	}

	@Test
	public void migrate_errorMigratingOneProyect_MigrationContinuesWithTheNextProject(){
		ByteArrayOutputStream errContent = new ByteArrayOutputStream();
		System.setErr(new PrintStream(errContent));
		
		CsvReaderFake csvReader=new CsvReaderFake();
		csvReader.projects=[new Project(),new Project(),new Project()]

		def callsNumber=0
		MigratorCreateGitRepository migrator=new MigratorCreateGitRepository(null,null,null,null,csvReader){
			void migrate(projecName,svnUrl,gitUrl){
				callsNumber++
				if(callsNumber==2)
					throw new Exception("Error en la migracion del segundo proyecto")
			}
		}
		migrator.migrate(new File("."))
		
		assertEquals(3,callsNumber)
	}
	
	class CsvReaderFake extends ProjectCsvReader{
		def projects
		List<Project> readProjects(projectsFile){
			return projects
		}
	}
}
