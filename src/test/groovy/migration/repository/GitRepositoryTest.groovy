package migration.repository

import static org.junit.Assert.*
import migration.repository.GitRepository;

import org.ajoberstar.grgit.Grgit
import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder
public class GitRepositoryTest {
	
	@Rule
	public TemporaryFolder temp=new TemporaryFolder();
	
	GitRepository gitRepository;
	
	def remoteRepo;
	def downloadDir;
	def localRepo;
	def git
	
	@Before
	public void setup() {
		gitRepository=new GitRepository()
		remoteRepo=temp.newFolder("repo.git")
		downloadDir=temp.newFolder("download")
		localRepo=new File(downloadDir.path,"localRepo")
		git = Grgit.init(dir: remoteRepo.path)
		initRepository()
	}
  
	@Test
	public void commitAndPush_PushRecursiveFiles(){
		gitRepository.clone("file://$remoteRepo", localRepo.path)
		createFiles()
		
		gitRepository.commitAndPush()
		
		verifyPushedFiles()
		
		gitRepository.close()
	}
	
	def initRepository(){
		def emptyFile=new File(remoteRepo,"init.txt")
		emptyFile.createNewFile();
		git.add(patterns: ["init.txt"])
		git.commit(message: 'Init Repository')
	}
	
	def createFiles(){
		new File(localRepo,"file1.txt").createNewFile();
		def dir1=new File(localRepo,"dir1")
		dir1.mkdirs()
		new File(dir1,"file2.txt").createNewFile();
	}
	
	def verifyPushedFiles(){
		def verifyRepo=new File(downloadDir.path,"verifyRepo")
		gitRepository=new GitRepository()
		gitRepository.clone("file://$remoteRepo", verifyRepo.path)
		
		def file1=new File(verifyRepo,"file1.txt")
		assertTrue(file1.exists())
		def file2=new File(new File(verifyRepo,"dir1"),"file2.txt")
		assertTrue(file2.exists())
	}
}