package migration.repository

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.junit.Assert.*;

class CleanProjectFilesTest {
	@Rule
	public TemporaryFolder temp=new TemporaryFolder();
	
	File projectDir;
	CleanProjectFiles cleanProjectFiles;
	
	@Before
	public void setup() {
		projectDir=temp.newFolder("project")
		cleanProjectFiles=new CleanProjectFiles();
	}
  
	@Test
	public void deleteAllFiles(){
		def anyFile=new File(projectDir,"anyFile.txt")
		anyFile.createNewFile()
		
		cleanProjectFiles.clean(projectDir)
		
		assertFalse(anyFile.exists())
	}

	@Test
	public void deleteAllDirectories(){
		def anyDir=new File(projectDir,"anyDir")
		anyDir.mkdir()
		def anySubdir=new File(anyDir,"anySubdir")
		anySubdir.mkdir()
		def anyFileInsideSubdir=new File(anySubdir,"anyFileInsideSubdir.txt")
		anyFileInsideSubdir.createNewFile()
		
		cleanProjectFiles.clean(projectDir)
		
		assertFalse(anyDir.exists())
	}
	
	@Test
	public void noEliminaDotGit(){
		def dotGit=new File(projectDir,".git")
		dotGit.mkdir()
		def anyGitSubdir=new File(dotGit,"anyGitSubdir")
		anyGitSubdir.mkdir()
		def anyFileInsideGitSubdir=new File(anyGitSubdir,"anyFileInsideGitSubdir.txt")
		anyFileInsideGitSubdir.createNewFile()
		
		cleanProjectFiles.clean(projectDir)
		
		assertTrue(dotGit.exists())
		assertTrue(anyGitSubdir.exists())
		assertTrue(anyFileInsideGitSubdir.exists())
	}
}
