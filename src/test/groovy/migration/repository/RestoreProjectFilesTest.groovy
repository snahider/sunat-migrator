package migration.repository

import static org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

class RestoreProjectFilesTest {
	@Rule
	public TemporaryFolder temp=new TemporaryFolder();
	
	
	File backupDir;
	File projectBackupDir;
	File projectDir;
	RestoreProjectFiles restoreProjectFiles;
	
	
	
	@Before
	public void setup() {
		projectDir=temp.newFolder("project")
		backupDir=temp.newFolder()
		projectBackupDir=new File(backupDir,"project")
		projectBackupDir.mkdir()
		restoreProjectFiles=new RestoreProjectFiles();
	}
  
	@Test
	public void restoreGitIgnore(){
		new File(projectBackupDir,".gitignore").createNewFile()
		
		restoreProjectFiles.restore(projectDir,projectBackupDir)
		
		def exists=new File(projectDir,".gitignore").exists()
		assertTrue(exists)
	}
	
	
	@Test
	public void restoreAnyParentGradleFile(){
		new File(projectBackupDir,"settings.gradle").createNewFile()
		new File(projectBackupDir,"weblogic.gradle").createNewFile()
		new File(projectBackupDir,"build.gradle").createNewFile()
		
		restoreProjectFiles.restore(projectDir,projectBackupDir)
		
		def exists=new File(projectDir,"settings.gradle").exists()
		assertTrue(exists)
		exists=new File(projectDir,"weblogic.gradle").exists()
		assertTrue(exists)
		exists=new File(projectDir,"build.gradle").exists()
		assertTrue(exists)
	}

	@Test
	public void restoreSubDirBuildGradle(){
		def subDir=new File(projectBackupDir,"model.jar")
		subDir.mkdir()
		new File(subDir,"build.gradle").createNewFile()
		
		restoreProjectFiles.restore(projectDir,projectBackupDir)
		
		def exists=new File(projectDir,"model.jar/build.gradle").exists()
		assertTrue(exists)
	}
	
//	@Test
//	public void restoreDotGit(){
//		def git=new File(projectBackupDir,".git")
//		git.mkdir()
//		def gitSubDir=new File(git,"gitSubDir")
//		gitSubDir.mkdir()
//		new File(gitSubDir,"file1.txt").createNewFile()
//		
//		restoreProjectFiles.restore(projectDir,projectBackupDir)
//		
//		def exists=new File(projectDir,".git").exists()
//		assertTrue(exists)
//		exists=new File(projectDir,".git/gitSubDir").exists()
//		assertTrue(exists)
//		exists=new File(projectDir,".git/gitSubDir/file1.txt").exists()
//		assertTrue(exists)
//	}
}
