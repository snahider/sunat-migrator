package migration.repository

import static org.junit.Assert.*

import org.junit.After
import org.junit.Rule;
import org.junit.Test
import org.junit.rules.TemporaryFolder;
import org.testng.annotations.ExpectedExceptions;

class MigratorUpdateGitRepositoryTest {
	
	@Rule
	public TemporaryFolder temp=new TemporaryFolder();
	
	@After
	public void revertErrStream(){
		PrintStream stream =new PrintStream(new FileOutputStream(FileDescriptor.err))
		System.setErr(new PrintStream(stream));
	}
	
	@Test
	public void migrate_invalidFile_throwsException(){
		MigratorUpdateGitRepository migrator=new MigratorUpdateGitRepository(null,null,null,null)
		def file=new File('fileNotExist')
		try{
			migrator.migrate(file)
			fail()
		}catch(e){
			assertEquals("No se ha encontrado el archivo fileNotExist",e.message)
		}
	}

	@Test
	public void migrate_errorMigratingOneProyect_MigrationContinuesWithTheNextProject(){
		ByteArrayOutputStream errContent = new ByteArrayOutputStream();
		System.setErr(new PrintStream(errContent));
		
		def workingDir=temp.newFolder()
		
		CsvReaderFake csvReader=new CsvReaderFake();
		csvReader.projects=[new Project(),new Project(),new Project()]

		def callsNumber=0
		MigratorUpdateGitRepository migrator=new MigratorUpdateGitRepository(workingDir,null,null,csvReader){
			void migrate(projecName,backupDir,svnUrl,gitUrl){
				callsNumber++
				if(callsNumber==2)
					throw new Exception("Error en la migracion del segundo proyecto")
			}
		}
		migrator.migrate(new File("."))
		
		assertEquals(3,callsNumber)
	}
	
	@Test
	public void migrate_siNoSeEncuentraElParentBuild_LanzaExcepcion(){
		def workingDir=temp.newFolder()
		MigratorUpdateGitRepository migrator=new MigratorUpdateGitRepository(workingDir,null,new GitRepositoryFake(),null)
		
		try{
			migrator.migrate("project", migrator, migrator, migrator)
			fail()
		}
		catch(NoParentBuildException e){
			
		}
	}
	
	class GitRepositoryFake extends GitRepository{
		void clone(url,downloadDir){
			def modelo=new File(downloadDir,"model.jar")
			modelo.mkdir()
		}
	}
	
	class CsvReaderFake extends ProjectCsvReader{
		def projects
		List<Project> readProjects(projectsFile){
			return projects
		}
	}
}
