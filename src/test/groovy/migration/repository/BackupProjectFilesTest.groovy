package migration.repository

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import static org.junit.Assert.*;

class BackupProjectFilesTest {
	@Rule
	public TemporaryFolder temp=new TemporaryFolder();
	
	
	File projectBackupDir;
	File projectDir;
	BackupProjectFiles backupProjectFiles;
	
	
	
	@Before
	public void setup() {
		projectDir=temp.newFolder("project")
		projectBackupDir=temp.newFolder()
		backupProjectFiles=new BackupProjectFiles();
	}
  
	@Test
	public void backupGitIgnore(){
		new File(projectDir,".gitignore").createNewFile()
		
		backupProjectFiles.backup(projectDir,projectBackupDir)
		
		def exists=new File(projectBackupDir,".gitignore").exists()
		assertTrue(exists)
	}
	
	
	@Test
	public void backupAnyParentGradleFile(){
		new File(projectDir,"settings.gradle").createNewFile()
		new File(projectDir,"weblogic.gradle").createNewFile()
		new File(projectDir,"build.gradle").createNewFile()
		
		backupProjectFiles.backup(projectDir,projectBackupDir)
		
		def exists=new File(projectBackupDir,"settings.gradle").exists()
		assertTrue(exists)
		exists=new File(projectBackupDir,"weblogic.gradle").exists()
		assertTrue(exists)
		exists=new File(projectBackupDir,"build.gradle").exists()
		assertTrue(exists)
	}
	
	@Test
	public void backupSubDirBuildGradle(){
		def subDir=new File(projectDir,"model.jar")
		subDir.mkdir()
		new File(subDir,"build.gradle").createNewFile()
		
		backupProjectFiles.backup(projectDir,projectBackupDir)
		
		def exists=new File(projectBackupDir,"model.jar/build.gradle").exists()
		assertTrue(exists)
	}
	
//	@Test
//	public void backupDotGit(){
//		def git=new File(projectDir,".git")
//		git.mkdir()
//		def gitSubDir=new File(git,"gitSubDir")
//		gitSubDir.mkdir()
//		new File(gitSubDir,"file1.txt").createNewFile()
//		
//		backupProjectFiles.backup(projectDir,projectBackupDir)
//		
//		def exists=new File(projectBackupDir,".git").exists()
//		assertTrue(exists)
//		exists=new File(projectBackupDir,".git/gitSubDir").exists()
//		assertTrue(exists)
//		exists=new File(projectBackupDir,".git/gitSubDir/file1.txt").exists()
//		assertTrue(exists)
//	}
	
}
