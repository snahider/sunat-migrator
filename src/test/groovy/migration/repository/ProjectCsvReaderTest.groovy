package migration.repository

import static org.junit.Assert.*

import java.nio.file.Path
import java.nio.file.Paths

import migration.repository.ProjectCsvReader;

import org.junit.Before
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.junit.Rule
import org.tmatesoft.svn.core.io.SVNRepositoryFactory
import org.tmatesoft.svn.core.SVNURL

public class ProjectCsvReaderTest {

	@Test
	public void readProjects_obtieneInformacionProyectosDeUnArchivoCSV(){
		ClassLoader classLoader = getClass().getClassLoader();
		File csv=new File(classLoader.getResource("migration/projects.txt").getFile())
		
		ProjectCsvReader projectCsv=new ProjectCsvReader();
		def projects=projectCsv.readProjects(csv)
		
		assertEquals(2,projects.size())
		assertEquals('project1',projects[0].name)
		assertEquals('http://svnhost.com/svn1',projects[0].svnUrl)
		assertEquals('http://githost.com/git1',projects[0].gitUrl)
		assertEquals('project2',projects[1].name)
		assertEquals('http://svnhost.com/svn2',projects[1].svnUrl)
		assertEquals('http://githost.com/git2',projects[1].gitUrl)
		
	}
}