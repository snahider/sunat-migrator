package migration.versiones

import static org.junit.Assert.*

import org.junit.Before
import org.junit.Test

public class VersionesParserTest {
	
	VersionesParser parser
	
	@Before
	public void setup() {
		parser=new VersionesParser()
	}
  
	@Test
	public void parseText_XmlSoloProyecto_retornaCadenaEnBlanco(){
		String mavenFile='''
		<project></project>'''
		
		def properties=parser.parseText(mavenFile)
		
		assertEquals("",properties)
	}
	
	@Test
	public void parseText_XmlSinPropiedades_retornaCadenaEnBlanco(){
		String mavenFile='''
		<project><properties>
		</properties></project>'''
		
		def properties=parser.parseText(mavenFile)
		
		assertEquals("",properties)
	}
	
	@Test
	public void parseText_XmlConUnaPropiedad_retornaLaPropiedad(){
		String mavenFile='''
		<project><properties>
			<jasper-version>3.7.3</jasper-version>
		</properties></project>'''
		
		def properties=parser.parseText(mavenFile)
		
		def expected="jasperVersion = 3.7.3"
		assertEquals(expected,properties)
	}
	
	@Test
	public void parseText_XmlConMultiplesPropiedades_retornaLasPropiedades(){
		String mavenFile='''
		<project><properties>
			<jasper-version>3.7.3</jasper-version>
			<grp-administracion2-tramite-sharedlib>procesos.administracion2.tramite.administracion2-tramite-sharedlib</grp-administracion2-tramite-sharedlib>
			<vrs-administracion2-tramite-sharedlib>2.0.1</vrs-administracion2-tramite-sharedlib>
		</properties></project>'''
		
		def properties=parser.parseText(mavenFile)
		
		def expected=
'''jasperVersion = 3.7.3
grpAdministracion2TramiteSharedlib = procesos.administracion2.tramite.administracion2-tramite-sharedlib
vrsAdministracion2TramiteSharedlib = 2.0.1'''
		assertEquals(expected,properties)
	}
}