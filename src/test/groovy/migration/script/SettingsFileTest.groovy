package migration.script

import static org.junit.Assert.*

import java.nio.file.Path
import java.nio.file.Paths

import org.junit.Before
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.junit.Rule

public class SettingsFileTest {

	@Rule
	public TemporaryFolder temp=new TemporaryFolder();

	@Test
	public void create_multipleProjects_createSettingsGradleeWithAllProjects(){
		File projectDir=temp.newFolder()
		File project1=new File(projectDir,"proyecto.jar")
		File project2=new File(projectDir,"proyecto.war")

		SettingsFile file= new SettingsFile()
		file.create(projectDir,[project1, project2]);

		File settings=new File(projectDir,"settings.gradle")
		assertEquals("include 'proyecto.jar', 'proyecto.war'",settings.text)
	}
}