package migration.script

import org.apache.commons.io.FileUtils

import static org.junit.Assert.*

import java.nio.file.Path
import java.nio.file.Paths

import org.junit.Before
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.junit.Rule

public class MigratorBuildTest {

	@Rule
	public TemporaryFolder temp=new TemporaryFolder();

	@Test
	public void getSubProjectsDirs_parentDir_returnsAllSubProjectsInParentDir(){
		File parentDir=temp.newFolder();
		File subProject1=new File(parentDir,"proyecto.jar")
		subProject1.mkdir()
		File subProject2=new File(parentDir,"proyecto.war")
		subProject2.mkdir()

		MigratorBuild migrator= new MigratorBuild(parentDir,null)
		def subProjectsDirs=migrator.getSubProjectsDirs(parentDir)

		assertEquals("proyecto.jar",subProjectsDirs[0].name)
		assertEquals("proyecto.war",subProjectsDirs[1].name)
	}
	
	@Test
	public void getSubProjectsDirs_invalidSubDirs_returnSubProjectsWithoutTheInvalidSubDirs(){
		File parentDir=temp.newFolder();
		File subProject1=new File(parentDir,"proyecto.jar")
		subProject1.mkdir()
		File subProject2=new File(parentDir,"proyecto.war")
		subProject2.mkdir()
		File gradleDir=new File(parentDir,".gradle")
		gradleDir.mkdir()
		File binDir=new File(parentDir,"bin")
		binDir.mkdir()
		
		MigratorBuild migrator= new MigratorBuild(parentDir,null)
		def subProjectsDirs=migrator.getSubProjectsDirs(parentDir)

		assertEquals("proyecto.jar",subProjectsDirs[0].name)
		assertEquals("proyecto.war",subProjectsDirs[1].name)
	}
}