package migration.script

import static org.junit.Assert.*
import org.apache.commons.io.FileUtils
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TemporaryFolder

public class BuildScriptFilesTest {

	@Rule
	public TemporaryFolder temp=new TemporaryFolder();

	File scriptsDir
	File jarScript
	File warScript
	File earScript
	File parentProjectDir
	File jarSubProjectDir
	File warSubProjectDir
	File earSubProjectDir

	BuildScriptFiles buildFile

	@Before
	public void setup(){
		scriptsDir=temp.newFolder()
		new File(scriptsDir,"build.gradle").createNewFile()
		new File(scriptsDir,"jar").mkdir()
		jarScript=new File(scriptsDir,"jar/build.gradle")
		jarScript.createNewFile()
		new File(scriptsDir,"war").mkdir()
		warScript=new File(scriptsDir,"war/build.gradle")
		warScript.createNewFile()
		new File(scriptsDir,"ear").mkdir()
		earScript=new File(scriptsDir,"ear/build.gradle")
		earScript.createNewFile()

		parentProjectDir=temp.newFolder()
		jarSubProjectDir=new File(parentProjectDir,"myproject.jar")
		jarSubProjectDir.mkdir()
		warSubProjectDir=new File(parentProjectDir,"myproject.war")
		warSubProjectDir.mkdir()
		earSubProjectDir=new File(parentProjectDir,"myproject.ear")
		earSubProjectDir.mkdir()

		buildFile=new BuildScriptFiles(parentProjectDir,scriptsDir);
	}

	@Test
	public void getSubModuleBuild_jarSubModule_returnsJarBuildScipt(){
		File scriptInSubProject=buildFile.getSubProjectBuild(jarSubProjectDir)

		assertEquals(jarScript.path,scriptInSubProject.path)
	}

	@Test
	public void getSubModuleBuild_warSubModule_returnsWarBuildScript(){
		File scriptInSubProject=buildFile.getSubProjectBuild(warSubProjectDir)

		assertEquals(new File(scriptsDir,"war/build.gradle").path,scriptInSubProject.path)
	}

	@Test
	public void getSubModuleBuild_earSubModule_returnsEarBuildScript(){
		File scriptInSubProject=buildFile.getSubProjectBuild(earSubProjectDir)

		assertEquals(new File(scriptsDir,"ear/build.gradle").path,scriptInSubProject.path)
	}

	@Test
	public void getSubModuleBuild_subModuleWithInvalidDirName_throwsException(){
		File subModuleDir=new File("projectWithoutExtension")

		try{
			File file=buildFile.getSubProjectBuild(subModuleDir)
			fail("Doesn't throws exception")
		}catch(e){
		}
	}

	@Test
	public void copyBuildScripts_subProjectsDirs_copyTheCorrectScriptForEachProjectType(){
		buildFile.copyBuildScripts(parentProjectDir,[
			jarSubProjectDir,
			warSubProjectDir,
			earSubProjectDir
		])

		File jarScript=new File(jarSubProjectDir,"build.gradle")
		assertTrue(jarScript.exists())
		File warScript=new File(warSubProjectDir,"build.gradle")
		assertTrue(warScript.exists())
		File earScript=new File(earSubProjectDir,"build.gradle")
		assertTrue(earScript.exists())
	}
	
	@Test
	public void copyBuildScripts_copyParentBuildScript(){
		buildFile.copyBuildScripts(parentProjectDir,[])
		
		File parentScript=new File(parentProjectDir,'build.gradle')
		assertTrue(parentScript.exists())
	}
}