package migration.script

import static org.junit.Assert.*

import java.nio.file.Path
import java.nio.file.Paths

import migration.dependencies.DependenciesParser;

import org.junit.Before
import org.junit.Test
import org.junit.rules.TemporaryFolder
import org.junit.Rule

public class DependenciesParserTest {
	
	DependenciesParser parser
	
	@Before
	public void setup() {
		parser=new DependenciesParser()
	}
  
	@Test
	public void parseText_XmlSoloProyecto_retornaCadenaEnBlanco(){
		String dependenciesMvn='''
		<project></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		assertEquals("",dependenciesGradle)
	}
	
	@Test
	public void parseText_XmlSinDependencias_retornaClosureVacio(){
		String dependenciesMvn='''
		<project><dependencies>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
}'''
		assertEquals(expected,dependenciesGradle)
	}
	
	@Test
	public void parseText_XmlConUnaDependenciaVacia_retornaClosureConUnaDependenciaVacia(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	compile group: '', name: '', version: '+'
}'''
		assertEquals(expected,dependenciesGradle)	
	}

	@Test
	public void parseText_XmlConUnaDependencia_RetornaClosureConLaDependencia(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
				<groupId>group</groupId> 
				<artifactId>artifact</artifactId>
				<scope>provided</scope>
				<version>1.0.0</version>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	compile group: 'group', name: 'artifact', version: '1.0.0'
}'''
		assertEquals(expected,dependenciesGradle)
	}
		
	@Test
	public void parseText_XmlConUnaDependenciaSinVersion_RetornaClosureConLaDependenciaConSimboloMas(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
				<groupId>group</groupId> 
				<artifactId>artifact</artifactId>
				<scope>provided</scope>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	compile group: 'group', name: 'artifact', version: '+'
}'''
		assertEquals(expected,dependenciesGradle)
	}

	@Test
	public void parseText_dependenciaConScopeTest_RetornaClosureConLaDependenciaConScopeTestCompile(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
				<groupId>group</groupId> 
				<artifactId>artifact</artifactId>
				<scope>test</scope>
				<version>1.0.0</version>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	testCompile group: 'group', name: 'artifact', version: '1.0.0'
}'''
		assertEquals(expected,dependenciesGradle)
	}
		
	@Test
	public void parseText_XmlConVariasDependencias_RetornaClosureConVariasDependencias(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
				<groupId>group-1</groupId> 
				<artifactId>artifact-1</artifactId>
				<scope>provided</scope>
			</dependency>
			<dependency>
				<groupId>group-2</groupId> 
				<artifactId>artifact-2</artifactId>
				<scope>provided</scope>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	compile group: 'group-1', name: 'artifact-1', version: '+'
	compile group: 'group-2', name: 'artifact-2', version: '+'
}'''
		assertEquals(expected,dependenciesGradle)
	}

	@Test
	public void parseText_XmlConGroupIdComoPropiedad_TransformaElGroupIdAUnaVariableDeVersiones(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
				<groupId>${grp-org-app}</groupId> 
				<artifactId>artifact</artifactId>
				<scope>provided</scope>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	compile group: versiones.grpOrgApp, name: 'artifact', version: versiones.vrsOrgApp
}'''
		assertEquals(expected,dependenciesGradle)
	}
	
	@Test
	public void parseText_ExcludeDependencies_RetornaElClosureConElExclude(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
				<groupId>${grp-org-app}</groupId> 
				<artifactId>artifact</artifactId>
				<scope>provided</scope>
				<exclusions>
				    <exclusion>
				    		<groupId>group-excluded</groupId>
				    		<artifactId>artifact-excluded</artifactId>
				    </exclusion>
				</exclusions>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	compile(group: versiones.grpOrgApp, name: 'artifact', version: versiones.vrsOrgApp){
		exclude group: 'group-excluded', module: 'artifact-excluded'
	}
}'''
		assertEquals(expected,dependenciesGradle)
	}

	@Test
	public void parseText_ExcludeDependenciesConGroupIdComoPropiedad_RetornaElClosureConElExclude(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
				<groupId>${grp-org-app}</groupId> 
				<artifactId>artifact</artifactId>
				<scope>provided</scope>
				<exclusions>
				    <exclusion>
				    		<groupId>${grp-org-app-excluded}</groupId>
				    		<artifactId>artifact-excluded</artifactId>
				    </exclusion>
				</exclusions>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	compile(group: versiones.grpOrgApp, name: 'artifact', version: versiones.vrsOrgApp){
		exclude group: versiones.grpOrgAppExcluded, module: 'artifact-excluded'
	}
}'''
		assertEquals(expected,dependenciesGradle)
	}
	
	@Test
	public void parseText_VariosExcludeDependencies_RetornaElClosureConTodosLosExcludes(){
		String dependenciesMvn='''
		<project><dependencies>
			<dependency>
				<groupId>${grp-org-app}</groupId> 
				<artifactId>artifact</artifactId>
				<scope>provided</scope>
				<exclusions>
				    <exclusion>
				    		<groupId>group-excluded-1</groupId>
				    		<artifactId>artifact-excluded-1</artifactId>
				    </exclusion>
				    <exclusion>
				    		<groupId>group-excluded-2</groupId>
				    		<artifactId>artifact-excluded-2</artifactId>
				    </exclusion>
				</exclusions>
			</dependency>
		</dependencies></project>'''
		
		def dependenciesGradle=parser.parseText(dependenciesMvn)
		
		def expected=
'''dependencies{
	compile(group: versiones.grpOrgApp, name: 'artifact', version: versiones.vrsOrgApp){
		exclude group: 'group-excluded-1', module: 'artifact-excluded-1'
		exclude group: 'group-excluded-2', module: 'artifact-excluded-2'
	}
}'''
		assertEquals(expected,dependenciesGradle)
	}
	
	/*
	 * TODO:
	 * - DependencyManagement tag
	 */
}