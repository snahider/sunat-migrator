package migration

import static org.hamcrest.CoreMatchers.instanceOf
import static org.junit.Assert.*

import org.junit.Test

public class MainApplicationTest {
	@Test
	public void siNoEncuentraNingunComandoLanzaException(){
		try {
			MainApplication main=new MainApplication(new File('workingDir'),new File('appHome'))
			main.run("commandoInvalido")
			fail()
		}
		catch(e){
			assertThat(e, instanceOf(CommandNotFoundException.class));
		}
	}
}